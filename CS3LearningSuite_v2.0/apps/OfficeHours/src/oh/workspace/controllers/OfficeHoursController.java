package oh.workspace.controllers;

import djf.modules.AppGUIModule;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_ADD_TA_BUTTON;
import static oh.OfficeHoursPropertyType.OH_NAME_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_EMAIL_TEXT_FIELD;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.transactions.AddTA_Transaction;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursController {

    OfficeHoursApp app;

    public OfficeHoursController(OfficeHoursApp initApp) {
        app = initApp;
    }

    public void processAddTA() {
        AppGUIModule gui = app.getGUIModule();
        TextField nameTF = (TextField) gui.getGUINode(OH_NAME_TEXT_FIELD);
        TextField emailTF = (TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD);
        String name = nameTF.getText();
        String email = emailTF.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name,email, "0");
        AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
        app.processTransaction(addTATransaction);

        // NOW CLEAR THE TEXT FIELDS, reset button disable
        nameTF.setText("");
        emailTF.setText("");
        ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
        nameTF.requestFocus();
    }
}
