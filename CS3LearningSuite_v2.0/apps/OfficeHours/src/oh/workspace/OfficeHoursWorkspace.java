package oh.workspace;

import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import oh.OfficeHoursApp;
import oh.OfficeHoursPropertyType;
import static oh.OfficeHoursPropertyType.*;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.transactions.AddOH_Transaction;
import oh.workspace.controllers.OfficeHoursController;
import oh.workspace.foolproof.OfficeHoursFoolproofDesign;
import static oh.workspace.style.OHStyle.*;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursWorkspace extends AppWorkspaceComponent {

    public OfficeHoursWorkspace(OfficeHoursApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();

        // INIT THE EVENT HANDLERS
        initControllers();

        // SETUP FOOLPROOF DESIGN FOR THIS APP
        initFoolproofDesign();
    }

    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppGUIModule gui = app.getGUIModule();
        AppNodesBuilder ohBuilder = gui.getNodesBuilder();
        
        

        // INIT THE HEADER ON THE LEFT
        VBox leftPane = ohBuilder.buildVBox(OH_LEFT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox tasHeaderBox = ohBuilder.buildHBox(OH_TAS_HEADER_PANE, leftPane, CLASS_OH_BOX, ENABLED);
        ohBuilder.buildLabel(OfficeHoursPropertyType.OH_TAS_HEADER_LABEL, tasHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        TableView<TeachingAssistantPrototype> taTable = ohBuilder.buildTableView(OH_TAS_TABLE_VIEW, leftPane, CLASS_OH_TABLE_VIEW, ENABLED);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        TableColumn nameColumn = ohBuilder.buildTableColumn(OH_NAME_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        
        TableColumn emailColumn = ohBuilder.buildTableColumn(OH_EMAIL_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        emailColumn.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/2.0));
        
        TableColumn slotsColumn = ohBuilder.buildTableColumn(OH_SLOTS_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        slotsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("timeSlot"));
        slotsColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        
        //sets prompttext
        nameColumn.textProperty().addListener((observable, oldValue, newValue) -> {
            ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).setPromptText(newValue);
        });
        emailColumn.textProperty().addListener((observable, oldValue, newValue) -> {
            ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)).setPromptText(newValue);
        });
        
        
        
        // ADD BOX FOR ADDING A TA
        HBox taBox = ohBuilder.buildHBox(OH_ADD_TA_PANE, leftPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildTextField(OH_NAME_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextField(OH_EMAIL_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextButton(OH_ADD_TA_BUTTON, taBox, CLASS_OH_BUTTON, ENABLED);
        
        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(taTable, Priority.ALWAYS);

        // INIT THE HEADER ON THE RIGHT
        VBox rightPane = ohBuilder.buildVBox(OH_RIGHT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox officeHoursHeaderBox = ohBuilder.buildHBox(OH_OFFICE_HOURS_HEADER_PANE, rightPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildLabel(OH_OFFICE_HOURS_HEADER_LABEL, officeHoursHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);

        // SETUP THE OFFICE HOURS TABLE
        TableView<TimeSlot> officeHoursTable = ohBuilder.buildTableView(OH_OFFICE_HOURS_TABLE_VIEW, rightPane, CLASS_OH_OFFICE_HOURS_TABLE_VIEW, ENABLED);
        TableColumn startTimeColumn = ohBuilder.buildTableColumn(OH_START_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN);
        TableColumn endTimeColumn = ohBuilder.buildTableColumn(OH_END_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN);
        TableColumn mondayColumn = ohBuilder.buildTableColumn(OH_MONDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn tuesdayColumn = ohBuilder.buildTableColumn(OH_TUESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn wednesdayColumn = ohBuilder.buildTableColumn(OH_WEDNESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn thursdayColumn = ohBuilder.buildTableColumn(OH_THURSDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn fridayColumn = ohBuilder.buildTableColumn(OH_FRIDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        startTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("startTime"));
        endTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("endTime"));
        mondayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("monday"));
        tuesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("tuesday"));
        wednesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("wednesday"));
        thursdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("thursday"));
        fridayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("friday"));
        officeHoursTable.getSelectionModel().setCellSelectionEnabled(true);
        for (int i = 0; i < officeHoursTable.getColumns().size(); i++) {
            ((TableColumn)officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0/7.0));
        }
  
        officeHoursTable.setOnMouseClicked(e->{
            TablePosition selected = (TablePosition)officeHoursTable.getSelectionModel().getSelectedCells().get(0);
            OfficeHoursData checker = (OfficeHoursData)app.getDataComponent();
            TableView tasTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
            
            
            int rowNum = selected.getRow();
            int colNum = selected.getColumn();
            TableColumn startTimeCol = (TableColumn) officeHoursTable.getColumns().get(0);
            TableColumn endTimeCol = (TableColumn) officeHoursTable.getColumns().get(1);
            String startTime = startTimeCol.getCellData(rowNum).toString();
            startTime = startTime.replace(":","_");
            TimeSlot slot = checker.getTimeSlot(startTime);
            if (!checker.isDayOfWeekColumn(colNum)) return;
            String day = checker.getColumnDayOfWeek(colNum).name();
            if (checker.isTASelected()) { //only run if a TA is selected
                TeachingAssistantPrototype ta = (TeachingAssistantPrototype) tasTable.getSelectionModel().getSelectedItem();
                day = day.toLowerCase();
                boolean add;
                if (slot.taInSlot(day, ta)) {
                     add = false;
                }
                else  {
                     add = true;
                }
                AddOH_Transaction addOHTransaction = new AddOH_Transaction(slot, day, ta, add); //pass in whether ta was added or removed
                app.processTransaction(addOHTransaction);
            }
        });

        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(officeHoursTable, Priority.ALWAYS);

        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, rightPane);
        sPane.setDividerPositions(.4);
        workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane)workspace).setCenter(sPane);
    }

    private void initControllers() {
        OfficeHoursController controller = new OfficeHoursController((OfficeHoursApp) app);
        
        AppGUIModule gui = app.getGUIModule();
        ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
        ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).setOnAction(e -> {
            controller.processAddTA();
            
        });
        
        ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)).textProperty().addListener((observable, oldValue, newValue) -> {
            OfficeHoursData checker = (OfficeHoursData)app.getDataComponent();
             if (checker.getTAWithEmail(newValue)!=null) {
                checker.selectTA(newValue,1);
            }
            Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(newValue);
            if (m.find()&& checker.getTAWithEmail(newValue)==null) {
                String nameTxt  =((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).getText();
                TeachingAssistantPrototype taExists = checker.getTAWithName(nameTxt);
                if (nameTxt.length()>1 && taExists==null){ //if name has value and no ta with this name.
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(false);
                }
                else {
                    ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
                }
            }
            else {
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
            }
        });
        ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD)).textProperty().addListener((observable, oldValue, newValue) -> {
            String nameTxt = newValue;
            OfficeHoursData checker = (OfficeHoursData)app.getDataComponent();
            if (checker.getTAWithName(nameTxt)!=null) {
                checker.selectTA(nameTxt,0);
            }
            Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
            String emailTxt = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD)).getText();
            Matcher m = p.matcher(emailTxt);
            if (m.find() && checker.getTAWithEmail(emailTxt)==null) {
                System.out.println(nameTxt);
                if (nameTxt.length()>1 && checker.getTAWithName(nameTxt)==null) {//if name has value and no ta with this name.
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(false);
                }
                //controller.processAddTA();
                else {
                    ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
                }
            }
            else {
                ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setDisable(true);
            }
        });
        ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setOnAction(e -> {
            controller.processAddTA();
        });
        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        // DON'T LET ANYONE SORT THE TABLES
        for (int i = 0; i < officeHoursTableView.getColumns().size(); i++) {
            ((TableColumn)officeHoursTableView.getColumns().get(i)).setSortable(false);
        }
    }

    private void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(OH_FOOLPROOF_SETTINGS,
                new OfficeHoursFoolproofDesign((OfficeHoursApp) app));
    }

    @Override
    public void showNewDialog() {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }
}
