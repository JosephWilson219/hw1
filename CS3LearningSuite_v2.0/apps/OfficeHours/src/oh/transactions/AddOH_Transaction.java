package oh.transactions;

import jtps.jTPS_Transaction;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;

/**
 *
 * @author McKillaGorilla
 */
public class AddOH_Transaction implements jTPS_Transaction {
    TimeSlot slot;
    String day;
    TeachingAssistantPrototype ta;
    boolean add;
    
    public AddOH_Transaction(TimeSlot time, String dy, TeachingAssistantPrototype t, boolean added) {
        slot =time;
        day = dy;
        ta = t;
        add = added;
    }

    @Override
    public void doTransaction() {
        if (add==true) {
            slot.addTA(day, ta);
        }
        else{
            slot.removeTA(day, ta);
        }
    }

    @Override
    public void undoTransaction() {
       if (add==false) { //if the ta was removed, add them on undo. Otherwise, remove them. 
            slot.addTA(day, ta);
        }
        else{
            slot.removeTA(day, ta);
        }
    }
}
